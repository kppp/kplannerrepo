USE [KWPIO]
GO
/****** Object:  Table [dbo].[AppUsers]    Script Date: 09/18/2021 11:29:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUsers](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[EmailAddress] [varchar](1000) NULL,
	[Password] [varchar](1000) NULL,
	[Phone] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_AppUsers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AppUsers] ON
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (1, N'Ali', N'Usman', N'ali_usman_idreesi@hotmail.com', N'rckn6977RC', N'', 1, CAST(0x0000AD32011B2AAE AS DateTime))
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (2, N'KP', N'Planner', N'ab@gmail.com', N'123456', N'', 1, CAST(0x0000AD3A00BCDA91 AS DateTime))
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (3, N'junaid', N'ashraf', N'junaid_hit@yahoo.com', N'bnkx1460BN', N'', 1, CAST(0x0000AD3B00C6E88F AS DateTime))
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (4, N'junaid_hit@yahoo.com', N'J', N'junaid_hit@yahoo.com', N'bnkx1460BN', N'', 1, CAST(0x0000AD4000829707 AS DateTime))
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (5, N'Junaid', N'Ashraf', N'junaid_hit@hotmail.com', N'procede', N'', 1, CAST(0x0000AD4000BDDB7E AS DateTime))
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (6, N'Sheraz', N'Javaid', N'sherazjavaid@yahoo.com', N'sheraz123', N'', 1, CAST(0x0000AD4000C22DC2 AS DateTime))
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (7, N'Rohail', N'Test', N'kplanner1234@gmail.com', N'123456', N'', 1, CAST(0x0000AD4700BDA02C AS DateTime))
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (8, N'Mansoor', N'Haq', N'mansoor.oyster@gmail.com', N'xlhu9272XL', N'', 1, CAST(0x0000AD52010E7067 AS DateTime))
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (9, N'kp', N'web', N'kp@gmail.com', N'123456', N'', 1, CAST(0x0000AD5D011450FC AS DateTime))
INSERT [dbo].[AppUsers] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Phone], [IsActive], [CreatedDate]) VALUES (10, N'rohail', N'testing', N'rohailmunda@gmail.com', N'urfh7934UR', N'', 1, CAST(0x0000AD5D0115F862 AS DateTime))
SET IDENTITY_INSERT [dbo].[AppUsers] OFF
/****** Object:  StoredProcedure [dbo].[sp_Account_SignUp]    Script Date: 09/18/2021 11:29:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Account_SignUp] 
(
	@ID BIGINT,
	@FirstName VARCHAR(100),
    @LastName VARCHAR(100),
    @EmailAddress VARCHAR(100),
    @Password VARCHAR(100),
    @Phone VARCHAR(50)
)
AS
BEGIN
	DECLARE @intResult BIGINT = -1;
	DECLARE @intCount BIGINT = 0;

	SELECT
		@intCount = COUNT(ID)
	FROM
		AppUsers
	WHERE
		EmailAddress = @EmailAddress

	IF @intCount = 0
	BEGIN
	 
		INSERT INTO [AppUsers]
		(
			FirstName,
			LastName,
			EmailAddress,
			Password,
			Phone,
			IsActive,
			CreatedDate
		)
		VALUES
		(
			@FirstName,
			@LastName,
			@EmailAddress,
			@Password,
			@Phone,
			1,
			GETDATE()
		)
	
		SELECT @intResult = IDENT_CURRENT('AppUsers')
	END

	SELECT @intResult AS ID;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Account_LoginIn]    Script Date: 09/18/2021 11:29:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Account_LoginIn]
(
	@EmailAddress VARCHAR(100),
    @Password VARCHAR(100)
)
AS
BEGIN
	SELECT
		*
	FROM
		AppUsers
	WHERE
		EmailAddress = @EmailAddress
		AND Password = @Password
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Account_IsUserNameExists]    Script Date: 09/18/2021 11:29:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Account_IsUserNameExists]
(
	@EmailAddress VARCHAR(100),
	@Password VARCHAR(100)
)
AS
BEGIN
	DECLARE @intResult BIGINT = 0;

	SELECT
		@intResult = COUNT(ID)
	FROM
		AppUsers
	WHERE
		EmailAddress = @EmailAddress;

	IF @intResult > 0
	BEGIN
		UPDATE AppUsers
		SET
			Password = @Password
		WHERE
			EmailAddress = @EmailAddress;

		SELECT
			*
		FROM
			AppUsers
		WHERE
			EmailAddress = @EmailAddress;
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Account_Change_Password]    Script Date: 09/18/2021 11:29:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Account_Change_Password]
(
	@EmailAddress VARCHAR(100),
    @Password VARCHAR(100),
	@NewPassword VARCHAR(100)
)
AS
BEGIN
	
	DECLARE @Count INT = 0;

	SELECT
		@Count = COUNT(ID)
	FROM
		AppUsers
	WHERE
		EmailAddress = @EmailAddress
		AND Password = @Password


	IF @Count > 0
	BEGIN
		UPDATE AppUsers
		SET
			Password = @NewPassword
		WHERE
			EmailAddress = @EmailAddress
			AND Password = @Password

		SELECT 1;
	END
	ELSE
	BEGIN
		SELECT -1;
	END
END
GO
