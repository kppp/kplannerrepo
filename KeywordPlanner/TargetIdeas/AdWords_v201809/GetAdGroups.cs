﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201809;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TargetIdeas.AdWords_v201809
{
    public class GetAdGroups
    {
        /// <summary>
        /// Get AdGroups
        /// </summary>
        /// <param name="user"> AdWordUser user = new AdWordUser(); </param>
        /// <param name="campaignId"> Campaign Id to get AdGroups </param>
        public List<KeyValuePair<string, string>> getAdGroups(AdWordsUser user, string campaignId)
        {
            List<KeyValuePair<string, string>> AdGroups = new List<KeyValuePair<string, string>>();

            using (AdGroupService adGroupService =
                (AdGroupService)user.GetService(AdWordsService.v201809.AdGroupService))
            {
                // Create the selector.
                Selector selector = new Selector()
                {
                    fields = new string[]
                    {
                        AdGroup.Fields.Id,
                        AdGroup.Fields.Name
                    },
                    predicates = new Predicate[]
                    {
                        Predicate.Equals(AdGroup.Fields.CampaignId, campaignId)
                    },
                    paging = Paging.Default,
                    ordering = new OrderBy[]
                    {
                        OrderBy.Asc(AdGroup.Fields.Name)
                    }
                };

                AdGroupPage page = new AdGroupPage();

                try
                {
                    do
                    {
                        // Get the ad groups.
                        page = adGroupService.get(selector);

                        // Display the results.
                        if (page != null && page.entries != null)
                        {
                            int i = selector.paging.startIndex;
                            foreach (AdGroup adGroup in page.entries)
                            {
                                AdGroups.Add(new KeyValuePair<string, string>(adGroup.id.ToString(),adGroup.name.ToString()));
                                i++;
                            }
                        }

                        // Note: You can also use selector.paging.IncrementOffsetBy(customPageSize)
                        selector.paging.IncreaseOffset();
                    } while (selector.paging.startIndex < page.totalNumEntries);

                    Console.WriteLine("Number of ad groups found: {0}", page.totalNumEntries);
                }
                catch (Exception e)
                {
                    throw new System.ApplicationException(e.Message);
                }

                return AdGroups;
            }
        }
    }
}
