﻿using Google.Api.Ads.AdWords.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TargetIdeas.AdWords_v201809;

namespace KeywordPlannerAPI.Controllers.AdWords_v201809
{
    public class AdWordCampaignController : ApiController
    {
        /// <summary>
        /// AdWord User = new AdWordUser();
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult POST(AdWordsUser user)
        {
            try
            {
                List<Tuple<string, string, string>> Campaigns = new GetCampaigns().GetCampaign(user);
                return Ok(Campaigns);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
