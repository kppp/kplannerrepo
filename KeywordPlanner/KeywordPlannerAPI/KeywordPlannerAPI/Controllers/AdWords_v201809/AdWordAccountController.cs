﻿using System;
using System.Web.Http;
using Google.Api.Ads.AdWords.Lib;
using KeywordPlannerAPI.Models;
using TargetIdeas.AdWords_v201809;
using TargetIdeas.Models;

namespace KeywordPlannerAPI.Controllers.AdWords_v201809
{
    public class AdWordAccountController : ApiController
    {
        /// <summary>
        /// AdWordUser user = new AdWordUser();
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult POST(AdWordsUser user)
        {
            try
            {
                UserAccount userAccount = new GetAccountHierarchy().GetCustomers(user);
                return Ok(userAccount);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
