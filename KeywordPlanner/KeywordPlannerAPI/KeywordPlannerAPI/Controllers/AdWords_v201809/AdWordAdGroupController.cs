﻿using Google.Api.Ads.AdWords.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TargetIdeas.AdWords_v201809;

namespace KeywordPlannerAPI.Controllers.AdWords_v201809
{
    public class AdWordAdGroupController : ApiController
    {
        /// <summary>
        /// Return Adgroups of the Specific Campaign
        /// </summary>
        /// <param name="user"> AdWordUser user = new AdWordUser(); </param>
        /// <param name="campaignId"> Campaign Id to get AdGroups </param>
        /// <returns> Keyvalue pair of adgroups </returns>
        [HttpPost]
        public IHttpActionResult POST([FromBody]AdWordsUser user, [FromUri]string CampaignId)
        {
            try
            {
                List<KeyValuePair<string, string>> adgroupsList = new GetAdGroups().getAdGroups(user, CampaignId);
                return Ok(adgroupsList);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
