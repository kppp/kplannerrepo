﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.Common.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace KeywordPlannerAPI.Controllers.AuthCallBack
{
    public class AuthCallBackController : Controller
    {
        /// <summary>
        /// Get with no parameter
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AuthCallBack()
        {
            string returnAppUrl = ConfigurationManager.AppSettings["returnAppUrl"];

            if (Session["UserInfo"] == null)
            {
                AdWordsAppConfig config = new AdWordsAppConfig();
                config.OAuth2RefreshToken = string.Empty;
                AdWordsUser user = new AdWordsUser();
                if (config.OAuth2Mode == OAuth2Flow.APPLICATION && string.IsNullOrEmpty(config.OAuth2RefreshToken))
                {
                    user = DoAuth2Configuration(config);
                    Session["UserInfo"] = user;
                }
            }

            //return this.Json(user, JsonRequestBehavior.AllowGet);

            //return Redirect(currntURL + "?statechanged=true");
            return Redirect(returnAppUrl);
        }
        [HttpGet]
        public JsonResult GetUserInfo()
        {
            if (Session["UserInfo"] != null)
            {
                return this.Json((AdWordsUser)Session["UserInfo"], JsonRequestBehavior.AllowGet);
            }
            return this.Json(new AdWordsUser(), JsonRequestBehavior.AllowGet);
        }

        private AdWordsUser DoAuth2Configuration(AdWordsAppConfig config)
        {
            AdWordsUser user = new AdWordsUser();
            // Since we use this page for OAuth callback also, we set the callback
            // url as the current page. For a non-web application, this will be null.
            config.OAuth2RedirectUri = Request.Url.GetLeftPart(UriPartial.Path);

            // Create an OAuth2 object for handling OAuth2 flow.
            OAuth2ProviderForApplications oAuth = new OAuth2ProviderForApplications(config);

            if (Request.Params["state"] == null)
            {
                // This is the first time this page is being loaded.
                // Set the state variable to any value that helps you recognize
                // when this url will be called by the OAuth2 server.
                oAuth.State = "callback";
                // Create an authorization url and redirect the user to that page.
                Response.Redirect(oAuth.GetAuthorizationUrl());
                //oAuth.FetchAccessAndRefreshTokens(Request.Params["code"]);
            }
            else if (Request.Params["state"] == "callback")
            {
                // This page was loaded because OAuth server did a callback.
                // Retrieve the authorization code from the url and use it to fetch
                // the access token. This call will also fetch the refresh token if
                // your mode is offline.
                oAuth.FetchAccessAndRefreshTokens(Request.Params["code"]);
                // Save the OAuth2 provider for future use. If you wish to save only
                // the values and restore the object later, then save
                // oAuth.RefreshToken, oAuth.AccessToken, oAuth.UpdatedOn and
                // oAuth.ExpiresIn.
                //
                // You can later restore the values as

                user.Config.OAuth2Mode = OAuth2Flow.APPLICATION;
                OAuth2ProviderForApplications oAuth1 = (user.OAuthProvider as OAuth2ProviderForApplications);
                oAuth1.Config.OAuth2RefreshToken = oAuth.Config.OAuth2RefreshToken;
                oAuth1.Config.OAuth2AccessToken = oAuth.Config.OAuth2AccessToken;
            }
            else
            {
                throw new Exception("Unknown state for OAuth callback.");
            }
            return user;
        }
    }
}