﻿using Google.Api.Ads.AdWords.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeywordPlannerAPI.Models
{
    public class ExpandedAdModel
    {
        public AdWordsUser User { get; set; }

        public string AdGroupId { get; set; }

        public List<TargetIdeas.Models.ExpandedAdModel> Ads { get; set; }

        public string[] Keywords { get; set; }
    }
}