﻿using KeywordPlannerAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace KeywordPlannerAPI.LocationHelper
{
    public sealed class CountryHelper
    {
        private static CountryHelper Instance = null;

        private List<CountryModel> ListModel { get; set; }

        private CountryHelper(){}
        public List<CountryModel> GetLocations()
        {
            try
            {
                var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                var t = @path + "\\LocationHelper\\CountryCode.json";
                ListModel = JsonConvert.DeserializeObject<List<CountryModel>>(File.ReadAllText(t));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return ListModel;
        }

        public static CountryHelper CountryInstance
        {
            get
            {
                if (Instance == null)
                {
                    Instance = new CountryHelper();
                }
                return Instance;
            }
        }
    }
}