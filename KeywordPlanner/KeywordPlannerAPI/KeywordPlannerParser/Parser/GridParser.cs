﻿using AdsApi = Google.Api.Ads.AdWords.v201809;
using KeywordPlannerAPI;
using KeywordPlannerParser.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordPlannerParser.Parser
{
    public class GridParser : ModelParser
    {
        public override dynamic Parse(dynamic targetIdeaPage)
        {
            List<GridModel> model = new List<GridModel>();
            try
            {
                for (int i = 0; i < targetIdeaPage.entries.Length; i++)
                {
                    int[] trendArray = new int[12];
                    GridModel m = new GridModel();
                    m.ID = i;
                    m.KeywordText = ((AdsApi.StringAttribute)targetIdeaPage.entries[i].data[0].value).value;
                    m.SearchVolume = ((AdsApi.LongAttribute)targetIdeaPage.entries[i].data[4].value).value;
                    if (m.SearchVolume != 0)
                    {
                        m.Competition = (Math.Round(((AdsApi.DoubleAttribute)targetIdeaPage.entries[i].data[1].value).value, 2)) * 100;
                        double COMPETITION_ROUNDED = Math.Round(((AdsApi.DoubleAttribute)targetIdeaPage.entries[i].data[1].value).value, 2);
                        if (COMPETITION_ROUNDED >= 0 && COMPETITION_ROUNDED < 0.3333)
                            m.CompetitionLabel = "LOW";
                        else if (COMPETITION_ROUNDED >= 0.3333 && COMPETITION_ROUNDED <= 0.6667)
                            m.CompetitionLabel = "MEDIUM";
                        else
                            m.CompetitionLabel = "HIGH";
                        for (int j = 0; j < m.MonthlyTargetedSearches.Length; j++)
                        {
                            m.MonthlyTargetedSearches[j] = new MonthlyTargetedSearches();
                            m.MonthlyTargetedSearches[j].SearchVolume = ((AdsApi.MonthlySearchVolumeAttribute)targetIdeaPage.entries[i].data[2].value).value[j].count;
                            m.MonthlyTargetedSearches[j].Month = ((AdsApi.MonthlySearchVolumeAttribute)targetIdeaPage.entries[i].data[2].value).value[j].month;
                            m.MonthlyTargetedSearches[j].Year = ((AdsApi.MonthlySearchVolumeAttribute)targetIdeaPage.entries[i].data[2].value).value[j].year;
                            m.Xaxis[j] = m.MonthlyTargetedSearches[j].Month.Value.ToMonth() + " " + Convert.ToString(m.MonthlyTargetedSearches[j].Year).Substring(2);
                            trendArray[j] = m.MonthlyTargetedSearches[j].Month.Value;
                            m.Yaxis[j] = m.MonthlyTargetedSearches[j].SearchVolume.Value;
                        }
                        if (((AdsApi.MoneyAttribute)targetIdeaPage.entries[i].data[3].value).value == null)
                        {
                            m.AverageCPC = Convert.ToDecimal(0.00);
                        }
                        else
                        {
                            m.AverageCPC = Math.Round(((((AdsApi.MoneyAttribute)targetIdeaPage.entries[i].data[3].value).value.microAmount) / (Convert.ToDecimal(1000000))), 2);
                        }
                        m.Ternd = m.Yaxis.Trend();
                        //long[] arr1 = m.Yaxis.Skip(0).Take(6).ToArray().ToRunningSum();
                        //double[] arr2 = arr1.ToRunningAvg(trendArray.Skip(0).Take(6).ToArray());
                        //double[] arr3 = m.Yaxis.Skip(0).Take(6).ToArray().ToRunningPercentGain(arr2);
                        //m.Ternd = Convert.ToString(arr3.Last()) + " " + "%";
                        Array.Reverse(m.Xaxis);
                        Array.Reverse(m.Yaxis);
                    }
                    model.Add(m);
                }
                return ( from m in model orderby m.SearchVolume descending select m).ToList();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
    }
}
