﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordPlannerParser.Parser
{
    public abstract class ModelParser
    {
        public abstract dynamic Parse(dynamic Array);
    }
}
