﻿using Google.Api.Ads.AdWords.Lib;
using System.Collections.Generic;

namespace KeywordPlannerAPI.Models
{
    public class UserAccountModel
    {
        public List<KeyValuePair<string, string>> Hierarchy { get; set; }

        public AdWordsUser User { get; set; }
    }
}