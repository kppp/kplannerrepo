﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201809;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TargetIdeas.AdWords_v201809
{
    public class GetCampaigns
    {
        public List<Tuple<string, string, string>> GetCampaign(AdWordsUser user)
        {
            List<Tuple<string, string, string>> Campaigns = new List<Tuple<string, string, string>>();
            using (CampaignService campaignService =
                (CampaignService)user.GetService(AdWordsService.v201809.CampaignService))
            {
                // Create the selector.
                Selector selector = new Selector()
                {
                    fields = new string[]
                    {
                        Campaign.Fields.Id,
                        Campaign.Fields.Name,
                        Campaign.Fields.Status
                    },
                    paging = Paging.Default
                };

                CampaignPage page = new CampaignPage();

                try
                {
                    do
                    {
                        // Get the campaigns.
                        page = campaignService.get(selector);

                        // Display the results.
                        if (page != null && page.entries != null)
                        {
                            int i = selector.paging.startIndex;
                            foreach (Campaign campaign in page.entries)
                            {
                                Campaigns.Add(new Tuple<string, string, string>(campaign.id.ToString(), campaign.name, campaign.status.ToString()));
                                i++;
                            }
                        }
                        selector.paging.IncreaseOffset();
                    } while (selector.paging.startIndex < page.totalNumEntries);

                    Console.WriteLine("Number of campaigns found: {0}", page.totalNumEntries);
                }
                catch (Exception e)
                {
                    throw new System.ApplicationException(e.Message);
                }
                return Campaigns;
            }
        }
    }
}
