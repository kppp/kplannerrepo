﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201809;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TargetIdeas.Models;

namespace TargetIdeas.AdWords_v201809
{
    public class GetAccountHierarchy
    {
        /// <summary>
        /// AdWordsUser =  new AdWordsUser();
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public UserAccount GetCustomers(AdWordsUser user)
        {
            List<KeyValuePair<string, string>> lstCustomers = new List<KeyValuePair<string, string>>();

            try
            {
                var customerservice = (CustomerService)user.GetService(AdWordsService.v201809.CustomerService);
                var customers = customerservice.getCustomers();

                int iCustomerIndex = 1;

                foreach (Customer customer in customers)
                {
                    string customerName;

                    if (string.IsNullOrEmpty(customer.descriptiveName))
                    {
                        customerName = iCustomerIndex.ToString() + " - Customer Name not available.";
                        iCustomerIndex++;
                    }
                    else
                    {
                        customerName = customer.descriptiveName;
                    }
                     ((AdWordsAppConfig)user.Config).ClientCustomerId =  customer.customerId.ToString();
                    ManagedCustomerTreeNode ChildNodes = GetChildNodes(user);
                    if(ChildNodes.ChildAccounts.Count == 0)
                    {
                        lstCustomers.Add(new KeyValuePair<string, string>(customer.customerId.ToString(), customerName));
                    }
                    else
                    {
                        for (int i = 0; i < ChildNodes.ChildAccounts.Count ; i++)
                        {
                            lstCustomers.Add(new KeyValuePair<string, string>(ChildNodes.ChildAccounts[i].Account.customerId.ToString(), ChildNodes.ChildAccounts[i].Account.name));
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new System.ApplicationException(ex.Message);
            }

            return new UserAccount { Hierarchy = lstCustomers , User = user };
        }

        public ManagedCustomerTreeNode GetChildNodes(AdWordsUser user)
        {
            using (ManagedCustomerService managedCustomerService =
                (ManagedCustomerService)user.GetService(AdWordsService.v201809
                    .ManagedCustomerService))
            {
                // Create selector.
                Selector selector = new Selector
                {
                    fields = new string[]
                    {
                        ManagedCustomer.Fields.CustomerId,
                        ManagedCustomer.Fields.Name,
                    },
                    paging = Paging.Default
                };

                // Map from customerId to customer node.
                Dictionary<long, ManagedCustomerTreeNode> customerIdToCustomerNode =
                    new Dictionary<long, ManagedCustomerTreeNode>();

                // Temporary cache to save links.
                List<ManagedCustomerLink> allLinks = new List<ManagedCustomerLink>();

                ManagedCustomerPage page = null;
                try
                {
                    do
                    {
                        page = managedCustomerService.get(selector);

                        if (page.entries != null)
                        {
                            // Create account tree nodes for each customer.
                            foreach (ManagedCustomer customer in page.entries)
                            {
                                ManagedCustomerTreeNode node = new ManagedCustomerTreeNode
                                {
                                    Account = customer
                                };
                                customerIdToCustomerNode.Add(customer.customerId, node);
                            }

                            if (page.links != null)
                            {
                                allLinks.AddRange(page.links);
                            }
                        }

                        selector.paging.IncreaseOffset();
                    } while (selector.paging.startIndex < page.totalNumEntries);

                    // For each link, connect nodes in tree.
                    foreach (ManagedCustomerLink link in allLinks)
                    {
                        ManagedCustomerTreeNode managerNode =
                            customerIdToCustomerNode[link.managerCustomerId];
                        ManagedCustomerTreeNode childNode =
                            customerIdToCustomerNode[link.clientCustomerId];
                        childNode.ParentNode = managerNode;
                        if (managerNode != null)
                        {
                            managerNode.ChildAccounts.Add(childNode);
                        }
                    }

                    // Find the root account node in the tree.
                    ManagedCustomerTreeNode rootNode = null;
                    foreach (ManagedCustomerTreeNode node in customerIdToCustomerNode.Values)
                    {
                        if (node.ParentNode == null)
                        {
                            rootNode = node;
                            break;
                        }
                    }

                    // Display account tree.
                    //Console.WriteLine("CustomerId, Name");
                    //Console.WriteLine(rootNode.ToTreeString(0, new StringBuilder()));
                    return rootNode;
                }
                catch (Exception e)
                {
                    throw new System.ApplicationException("Failed to create ad groups.", e);
                }
            }
        }

        /// <summary>
        /// Example implementation of a node that would exist in an account tree. 
        /// </summary>
        public class ManagedCustomerTreeNode
        {
            /// <summary>
            /// The parent node.
            /// </summary>
            private ManagedCustomerTreeNode parentNode;

            /// <summary>
            /// The account associated with this node.
            /// </summary>
            private ManagedCustomer account;

            /// <summary>
            /// The list of child accounts.
            /// </summary>
            private List<ManagedCustomerTreeNode> childAccounts =
                new List<ManagedCustomerTreeNode>();

            /// <summary>
            /// Gets or sets the parent node.
            /// </summary>
            public ManagedCustomerTreeNode ParentNode
            {
                get { return parentNode; }
                set { parentNode = value; }
            }

            /// <summary>
            /// Gets or sets the account.
            /// </summary>
            public ManagedCustomer Account
            {
                get { return account; }
                set { account = value; }
            }

            /// <summary>
            /// Gets the child accounts.
            /// </summary>
            public List<ManagedCustomerTreeNode> ChildAccounts
            {
                get { return childAccounts; }
            }

            /// <summary>
            /// Returns a <see cref="System.String"/> that represents this instance.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String"/> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                return string.Format("{0}, {1}", account.customerId, account.name);
            }

            /// <summary>
            /// Returns a string representation of the current level of the tree and
            /// recursively returns the string representation of the levels below it.
            /// </summary>
            /// <param name="depth">The depth of the node.</param>
            /// <param name="sb">The String Builder containing the tree
            /// representation.</param>
            /// <returns>The tree string representation.</returns>
            public StringBuilder ToTreeString(int depth, StringBuilder sb)
            {
                sb.Append('-', depth * 2);
                sb.Append(this);
                sb.AppendLine();
                foreach (ManagedCustomerTreeNode childAccount in childAccounts)
                {
                    childAccount.ToTreeString(depth + 1, sb);
                }

                return sb;
            }
        }
    }
}
