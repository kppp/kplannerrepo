﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201809;
using System;
using System.Collections.Generic;
using TargetIdeas.Models;

namespace TargetIdeas.AdWords_v201809
{
    public class AddExpandedTextAds
    {
        /// <summary>
        /// Add Ads in the Adgroups
        /// </summary>
        /// <param name="user"> The AdWord User</param>
        /// <param name="AdGroupId"> AdGroup Id</param>
        /// <param name="ads"> Ads Model list </param>
        /// <param name="Keywords"> keywords list </param>
        /// <returns></returns>
        public bool addExpandedTextAds(AdWordsUser user, string AdGroupId, List<ExpandedAdModel> ads, string[] Keywords)
        {
            bool adsResult = false;
            bool isKeywordsAdded = new AddKeywords().AddKeyword(user, AdGroupId, Keywords);
            if (isKeywordsAdded)
            {
                using (AdGroupAdService adGroupAdService =
                (AdGroupAdService)user.GetService(AdWordsService.v201809.AdGroupAdService))
                {
                    List<AdGroupAdOperation> operations = new List<AdGroupAdOperation>();

                    for (int i = 0; i < ads.Count ; i++)
                    {
                        // Create the expanded text ad.
                        ExpandedTextAd expandedTextAd = new ExpandedTextAd
                        {
                            headlinePart1 = ads[i].HeadLinePart1,
                            headlinePart2 = ads[i].HeadLinePart2,
                            headlinePart3 = ads[i].HeadLinePart3,
                            description = ads[i].Description1,
                            description2 = ads[i].Description2,
                            finalUrls = ads[i].FinalUrls.Split(',')
                        };

                        AdGroupAd expandedTextAdGroupAd = new AdGroupAd
                        {
                            adGroupId = long.Parse(AdGroupId),
                            ad = expandedTextAd,

                            // Optional: Set the status.
                            status = AdGroupAdStatus.PAUSED
                        };

                        // Create the operation.
                        AdGroupAdOperation operation = new AdGroupAdOperation
                        {
                            @operator = Operator.ADD,
                            operand = expandedTextAdGroupAd
                        };

                        operations.Add(operation);
                    }

                    AdGroupAdReturnValue retVal = null;

                    try
                    {
                        // Create the ads.
                        retVal = adGroupAdService.mutate(operations.ToArray());

                        // Display the results.
                        if (retVal != null && retVal.value != null)
                        {
                            foreach (AdGroupAd adGroupAd in retVal.value)
                            {
                                adsResult = true;
                            }
                        }
                        else
                        {
                            adsResult = false;
                        }

                        adGroupAdService.Close();
                    }
                    catch (Exception e)
                    {
                        throw new System.ApplicationException(e.Message);
                    }
                }
            }
            return adsResult;
        }
    }
}
