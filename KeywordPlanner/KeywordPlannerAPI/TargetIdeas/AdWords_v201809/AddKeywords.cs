﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201809;
using System;
using System.Collections.Generic;

namespace TargetIdeas.AdWords_v201809
{
    public class AddKeywords
    {
        /// <summary>
        /// Add the Keywords in the AdGroup
        /// </summary>
        /// <param name="user"> AdWordUser user = new AdWordUser(); </param>
        /// <param name="AdGroupId"> Adgrooup id in which keywords are added </param>
        /// <param name="Keywords"> List of Keywords </param>
        /// <returns></returns>
        public bool AddKeyword(AdWordsUser user, string AdGroupId, string[] Keywords)
        {
            bool result = false;

            using (AdGroupCriterionService adGroupCriterionService = (AdGroupCriterionService)user.GetService(AdWordsService.v201809.AdGroupCriterionService))
            {
                List<AdGroupCriterionOperation> operations = new List<AdGroupCriterionOperation>();

                foreach (string keywordText in Keywords)
                {
                    // Create the keyword.
                    Keyword keyword = new Keyword
                    {
                        text = keywordText,
                        matchType = KeywordMatchType.BROAD
                    };

                    // Create the biddable ad group criterion.
                    BiddableAdGroupCriterion keywordCriterion = new BiddableAdGroupCriterion
                    {
                        adGroupId = long.Parse(AdGroupId),
                        criterion = keyword,

                        // Optional: Set the user status.
                        //userStatus = UserStatus.PAUSED,

                        //// Optional: Set the keyword destination url.
                        //finalUrls = new UrlList()
                        //{
                        //    urls = new string[]
                        //    {
                        //        "http://example.com/mars/cruise/?kw=" +
                        //        HttpUtility.UrlEncode(keywordText)
                        //    }
                        //}
                    };

                    // Create the operations.
                    AdGroupCriterionOperation operation = new AdGroupCriterionOperation
                    {
                        @operator = Operator.ADD,
                        operand = keywordCriterion
                    };

                    operations.Add(operation);
                }

                try
                {
                    // Create the keywords.
                    AdGroupCriterionReturnValue retVal = adGroupCriterionService.mutate(operations.ToArray());

                    // Display the results.
                    if (retVal != null && retVal.value != null)
                    {
                        foreach (AdGroupCriterion adGroupCriterion in retVal.value)
                        {
                            // If you are adding multiple type of criteria, then you may need to
                            // check for
                            //
                            // if (adGroupCriterion is Keyword) { ... }
                            //
                            // to identify the criterion type.
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                catch (Exception e)
                {
                    throw new System.ApplicationException(e.Message);
                }
                return result;
            }
        }
    }
}
