﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201809;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeywordPlannerAPI
{
    class AdwordsServiceFactory
    {
        AdWordsUser user = null;

        ICampaignService objService = null;
        dynamic page = null;

        public AdwordsServiceFactory(AdWordsUser usr)
        {
            user = usr;
        }
        private ICampaignService SetServiceObject(string ServiceType)
        {
            switch (ServiceType)
            {
                case "CampaignService":
                    objService = (CampaignService)user.GetService(AdWordsService.v201809.CampaignService);
                    page = new CampaignPage();
                    break;
                default:
                    throw new Exception("The Service is not defined in AdwordsServiceFactory class");
            }

            return objService;
        }

        public dynamic GetData(string ServiceType, string IDFieldName, string IDFieldValues, string MappingFieldName)
        {
            // Get the CampaignService.
            objService = SetServiceObject(ServiceType);

            // Create the query.
            string query = string.Format("SELECT {0} Where {1} in [{2}]", MappingFieldName, IDFieldName, IDFieldValues);

            try
            {
                // Get the campaigns.
                return objService.query(query);
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to retrieve campaigns", e);
            }
        }
    }
}
