﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TargetIdeas.Models
{
    public class ExpandedAdModel
    {
        public string HeadLinePart1 { get; set; }

        public string HeadLinePart2 { get; set; }

        public string HeadLinePart3 { get; set; }

        public string Description1 { get; set; }

        public string Description2 { get; set; }

        public string FinalUrls { get; set; }
    }
}