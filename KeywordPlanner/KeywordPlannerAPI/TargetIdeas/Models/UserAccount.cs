﻿using Google.Api.Ads.AdWords.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TargetIdeas.Models
{
    public class UserAccount
    {
        public List<KeyValuePair<string, string>> Hierarchy { get; set; }

        public AdWordsUser User { get; set; }
    }
}
