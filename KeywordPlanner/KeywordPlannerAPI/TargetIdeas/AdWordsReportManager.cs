﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Api.Ads.AdWords.v201809;

namespace KeywordPlannerAPI
{
    class AdWordsReportManager
    {
        public static ReportDefinitionReportType GetReportType(string ReportType)
        {
            switch (ReportType)
            {
                case "ACCOUNT_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.ACCOUNT_PERFORMANCE_REPORT;
                case "AD_CUSTOMIZERS_FEED_ITEM_REPORT":
                    return ReportDefinitionReportType.AD_CUSTOMIZERS_FEED_ITEM_REPORT;
                case "ADGROUP_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.ADGROUP_PERFORMANCE_REPORT;
                case "AD_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.AD_PERFORMANCE_REPORT;
                case "AGE_RANGE_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.AGE_RANGE_PERFORMANCE_REPORT;
                case "AUDIENCE_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.AUDIENCE_PERFORMANCE_REPORT;
                case "AUTOMATIC_PLACEMENTS_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.AUTOMATIC_PLACEMENTS_PERFORMANCE_REPORT;
                case "BID_GOAL_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.BID_GOAL_PERFORMANCE_REPORT;
                case "BUDGET_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.BUDGET_PERFORMANCE_REPORT;
                case "CALL_METRICS_CALL_DETAILS_REPORT":
                    return ReportDefinitionReportType.CALL_METRICS_CALL_DETAILS_REPORT;
                case "CAMPAIGN_AD_SCHEDULE_TARGET_REPORT":
                    return ReportDefinitionReportType.CAMPAIGN_AD_SCHEDULE_TARGET_REPORT;
                case "CAMPAIGN_CRITERIA_REPORT":
                    return ReportDefinitionReportType.CAMPAIGN_CRITERIA_REPORT;
                case "CAMPAIGN_GROUP_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.CAMPAIGN_GROUP_PERFORMANCE_REPORT;
                case "CAMPAIGN_LOCATION_TARGET_REPORT":
                    return ReportDefinitionReportType.CAMPAIGN_LOCATION_TARGET_REPORT;
                case "CAMPAIGN_NEGATIVE_KEYWORDS_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.CAMPAIGN_NEGATIVE_KEYWORDS_PERFORMANCE_REPORT;
                case "CAMPAIGN_NEGATIVE_LOCATIONS_REPORT":
                    return ReportDefinitionReportType.CAMPAIGN_NEGATIVE_LOCATIONS_REPORT;
                case "CAMPAIGN_NEGATIVE_PLACEMENTS_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.CAMPAIGN_NEGATIVE_PLACEMENTS_PERFORMANCE_REPORT;
                case "CAMPAIGN_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.CAMPAIGN_PERFORMANCE_REPORT;
                case "CAMPAIGN_SHARED_SET_REPORT":
                    return ReportDefinitionReportType.CAMPAIGN_SHARED_SET_REPORT;
                case "CLICK_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.CLICK_PERFORMANCE_REPORT;
                case "CREATIVE_CONVERSION_REPORT":
                    return ReportDefinitionReportType.CREATIVE_CONVERSION_REPORT;
                case "CRITERIA_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.CRITERIA_PERFORMANCE_REPORT;
                //case "DESTINATION_URL_REPORT":
                //    return ReportDefinitionReportType.DESTINATION_URL_REPORT;
                case "DISPLAY_KEYWORD_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.DISPLAY_KEYWORD_PERFORMANCE_REPORT;
                case "DISPLAY_TOPICS_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.DISPLAY_TOPICS_PERFORMANCE_REPORT;
                case "FINAL_URL_REPORT":
                    return ReportDefinitionReportType.FINAL_URL_REPORT;
                case "GENDER_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.GENDER_PERFORMANCE_REPORT;
                case "GEO_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.GEO_PERFORMANCE_REPORT;
                case "KEYWORDLESS_CATEGORY_REPORT":
                    return ReportDefinitionReportType.KEYWORDLESS_CATEGORY_REPORT;
                case "KEYWORDLESS_QUERY_REPORT":
                    return ReportDefinitionReportType.KEYWORDLESS_QUERY_REPORT;
                case "KEYWORDS_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.KEYWORDS_PERFORMANCE_REPORT;
                case "LABEL_REPORT":
                    return ReportDefinitionReportType.LABEL_REPORT;
                case "PAID_ORGANIC_QUERY_REPORT":
                    return ReportDefinitionReportType.PAID_ORGANIC_QUERY_REPORT;
                case "PARENTAL_STATUS_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.PARENTAL_STATUS_PERFORMANCE_REPORT;
                case "PLACEHOLDER_FEED_ITEM_REPORT":
                    return ReportDefinitionReportType.PLACEHOLDER_FEED_ITEM_REPORT;
                case "PLACEHOLDER_REPORT":
                    return ReportDefinitionReportType.PLACEHOLDER_REPORT;
                case "PLACEMENT_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.PLACEMENT_PERFORMANCE_REPORT;
                case "PRODUCT_PARTITION_REPORT":
                    return ReportDefinitionReportType.PRODUCT_PARTITION_REPORT;
                case "SEARCH_QUERY_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.SEARCH_QUERY_PERFORMANCE_REPORT;
                case "SHARED_SET_CRITERIA_REPORT":
                    return ReportDefinitionReportType.SHARED_SET_CRITERIA_REPORT;
                case "SHARED_SET_REPORT":
                    return ReportDefinitionReportType.SHARED_SET_REPORT;
                case "SHOPPING_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.SHOPPING_PERFORMANCE_REPORT;
                case "TOP_CONTENT_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.TOP_CONTENT_PERFORMANCE_REPORT;
                case "URL_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.URL_PERFORMANCE_REPORT;
                case "USER_AD_DISTANCE_REPORT":
                    return ReportDefinitionReportType.USER_AD_DISTANCE_REPORT;
                case "VIDEO_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.VIDEO_PERFORMANCE_REPORT;
                case "LANDING_PAGE_REPORT":
                    return ReportDefinitionReportType.LANDING_PAGE_REPORT;
                case "MARKETPLACE_PERFORMANCE_REPORT":
                    return ReportDefinitionReportType.MARKETPLACE_PERFORMANCE_REPORT;

                default:
                    throw new Exception("Report type not defined in GetReportType factory method. Please define report type.");
            }
        }

        public static ReportDefinitionDateRangeType GetDateRangeType(int DateRangeIndex)
        {
            switch (DateRangeIndex)
            {
                case 0:
                    return ReportDefinitionDateRangeType.TODAY;
                case 1:
                    return ReportDefinitionDateRangeType.YESTERDAY;
                case 2:
                    return ReportDefinitionDateRangeType.LAST_7_DAYS;
                case 3:
                    return ReportDefinitionDateRangeType.LAST_WEEK;
                case 4:
                    return ReportDefinitionDateRangeType.LAST_BUSINESS_WEEK;
                case 5:
                    return ReportDefinitionDateRangeType.THIS_MONTH;
                case 6:
                    return ReportDefinitionDateRangeType.LAST_MONTH;
                case 7:
                    return ReportDefinitionDateRangeType.ALL_TIME;
                case 8:
                    return ReportDefinitionDateRangeType.CUSTOM_DATE;
                case 9:
                    return ReportDefinitionDateRangeType.LAST_14_DAYS;
                case 10:
                    return ReportDefinitionDateRangeType.LAST_30_DAYS;
                case 11:
                    return ReportDefinitionDateRangeType.THIS_WEEK_SUN_TODAY;
                case 12:
                    return ReportDefinitionDateRangeType.THIS_WEEK_MON_TODAY;
                case 13:
                    return ReportDefinitionDateRangeType.LAST_WEEK_SUN_SAT;
                default:
                    return ReportDefinitionDateRangeType.LAST_7_DAYS;
            }
        }
    }
}
