﻿using System;
using System.Collections.Generic;
using Google.Api.Ads.AdWords.v201809;
using Google.Api.Ads.AdWords.Lib;
using System.Configuration;
using System.Threading;

namespace KeywordPlannerAPI
{
    public class BLReport
    {
        public TargetingIdeaPage PopulateCompetitionData(string clientCustomerID, string[] Queries, int type, long languageId, long locationId, bool targetGoogleSearch, bool targetSearchNetwork, string[] excludeKeywordsList, bool isKeyword)
        {
            #region USER
            var user2 = new AdWordsUser();
            (user2.Config as AdWordsAppConfig).IncludeZeroImpressions = true;
            (user2.Config as AdWordsAppConfig).ClientCustomerId = clientCustomerID;
            #endregion USER

            using (TargetingIdeaService targetingIdeaService = (TargetingIdeaService)user2.GetService(AdWordsService.v201809.TargetingIdeaService))
            {
                // Create selector.
                TargetingIdeaSelector selector = new TargetingIdeaSelector();
                if (type == 3)
                    selector.requestType = RequestType.STATS;
                else
                    selector.requestType = RequestType.IDEAS;

                selector.ideaType = IdeaType.KEYWORD;

                selector.requestedAttributeTypes = new AttributeType[] {
                    AttributeType.KEYWORD_TEXT,
                    AttributeType.SEARCH_VOLUME,
                    AttributeType.AVERAGE_CPC,
                    AttributeType.COMPETITION,
                    AttributeType.TARGETED_MONTHLY_SEARCHES
                };

                List<SearchParameter> searchParameters = new List<SearchParameter>();

                // Create related to query search parameter.
                if(isKeyword == true)
                {
                    RelatedToQuerySearchParameter relatedToQuerySearchParameter = new RelatedToQuerySearchParameter();
                    relatedToQuerySearchParameter.queries = Queries; //new String[] {"+AirLink +GX450" };
                    searchParameters.Add(relatedToQuerySearchParameter);
                }
                else
                {
                    RelatedToUrlSearchParameter relatedToUrlSearchParameter = new RelatedToUrlSearchParameter();
                    relatedToUrlSearchParameter.urls = Queries;
                    searchParameters.Add(relatedToUrlSearchParameter);
                }

                LanguageSearchParameter languageParameter = new LanguageSearchParameter();
                Language lang = new Language();
                lang.id = languageId;
                languageParameter.languages = new Language[] { lang };
                searchParameters.Add(languageParameter);


                LocationSearchParameter location = new LocationSearchParameter();
                Location l = new Location();
                l.id = locationId;// 2586;                
                location.locations = new Location[] { l };
                searchParameters.Add(location);

                // Add network search parameter (optional).
                NetworkSetting networkSetting = new NetworkSetting();
                networkSetting.targetGoogleSearch = targetGoogleSearch;
                networkSetting.targetSearchNetwork = targetSearchNetwork;
                //networkSetting.targetContentNetwork = targetContentNetwork;
                //networkSetting.targetPartnerSearchNetwork = targetPartnerSearchNetwork;

                NetworkSearchParameter networkSearchParameter = new NetworkSearchParameter();
                networkSearchParameter.networkSetting = networkSetting;
                searchParameters.Add(networkSearchParameter);

                //excluded Keywords Lits
                IdeaTextFilterSearchParameter ideaTextFilterSearchParameter = new IdeaTextFilterSearchParameter();
                ideaTextFilterSearchParameter.excluded = excludeKeywordsList;
                searchParameters.Add(ideaTextFilterSearchParameter);

                // Set the search parameters.
                selector.searchParameters = searchParameters.ToArray();
                // Set selector paging (required for targeting idea service). 

                selector.paging = Paging.Default;
                //selector.paging = Paging.Default;
                selector.paging.numberResults = 700;

                TargetingIdeaPage page2 = new TargetingIdeaPage();
                int retryCount = 0;
                int NUM_RETRIES = Convert.ToInt32(ConfigurationManager.AppSettings["targetIdeaRetryCount"]);

                try
                {
                    while (retryCount < NUM_RETRIES)
                    {
                        try
                        {
                            do
                            {
                                // Get related keywords.
                                page2 = targetingIdeaService.get(selector);

                                // Display related keywords.
                                if (page2.entries != null && page2.entries.Length > 0)
                                {
                                    int i = selector.paging.startIndex;
                                    foreach (TargetingIdea targetingIdea in page2.entries)
                                    {
                                        Dictionary<AttributeType, Google.Api.Ads.AdWords.v201809.Attribute> ideas = targetingIdea.data.ToDict();

                                        string KEYWORD_TEXT = (ideas[AttributeType.KEYWORD_TEXT] as StringAttribute).value;

                                        MonthlySearchVolume[] TARGETED_MONTHLY_SEARCHES = (ideas[AttributeType.TARGETED_MONTHLY_SEARCHES] as MonthlySearchVolumeAttribute).value;

                                        long SEARCH_VOLUME = (ideas[AttributeType.SEARCH_VOLUME] as LongAttribute).value;

                                        Money AVERAGE_CPC = (ideas[AttributeType.AVERAGE_CPC] as MoneyAttribute).value;

                                        double COMPETITION = (ideas[AttributeType.COMPETITION] as DoubleAttribute).value;

                                        i++;
                                    }

                                }
                                selector.paging.IncreaseOffset();
                            } while (selector.paging.startIndex < page2.totalNumEntries);

                            return page2;
                        }
                        catch (AdWordsApiException e)
                        {
                            // Handle API errors.
                            ApiException innerException = e.ApiException as ApiException;
                            foreach (ApiError apiError in innerException.errors)
                            {
                                // Handle rate exceeded errors.
                                RateExceededError rateExceededError = (RateExceededError)apiError;
                                int seconds = rateExceededError.retryAfterSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["rateLimitDelayinSeconds"]);
                                Thread.Sleep(seconds);
                                retryCount = retryCount + 1;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new System.ApplicationException("Failed to validate keywords.", e);
                }

                return page2;
            }
        }
    }
}
