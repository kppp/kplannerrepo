﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using KeywordPlannerAPI.Models;
using KeywordPlannnerAPI.Helper;
using System.Net.Mail;
using System.Configuration;
using System.Text;
using System.Web;

namespace KeywordPlannerAPI.Controllers
{
    public class AccountController : ApiController
    {
        [Route("api/Account/SignUp")]
        [HttpPost]
        public IHttpActionResult SignUp(AccountModel accountModel)
        {
            SqlDataAccess _sqlDataAccess = new SqlDataAccess();

            string execCommand = "sp_Account_SignUp";
            int intID = 0;
            string strShowResults = "0";
            int intUserRemainingCredits = 0;
            bool blnIsFree = true;

            DataRow dr = null;
            SqlCommand _sql = _sqlDataAccess.GetCommand(execCommand, CommandType.StoredProcedure);

            _sql.Parameters.Add(new SqlParameter("ID", accountModel.ID));
            _sql.Parameters.Add(new SqlParameter("FirstName", accountModel.FirstName));
            _sql.Parameters.Add(new SqlParameter("LastName", accountModel.LastName));
            _sql.Parameters.Add(new SqlParameter("EmailAddress", accountModel.EmailAddress));
            _sql.Parameters.Add(new SqlParameter("Password", accountModel.Password));
            _sql.Parameters.Add(new SqlParameter("Phone", accountModel.Phone));


            DataTable dataTable = _sqlDataAccess.Execute(_sql);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                dr = dataTable.Rows[0];
                intID = Convert.ToInt32(dr["ID"]);
            }

            string strReturnValue = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                ID = intID
            });




            return Ok(strReturnValue);
        }

        [Route("api/Account/Login")]
        [HttpPost]
        public IHttpActionResult Login(AccountModel accountModel)
        {
            SqlDataAccess _sqlDataAccess = new SqlDataAccess();

            string execCommand = "sp_Account_LoginIn";
            int intID = 0;
            string strShowResults = "0";
            int intUserRemainingCredits = 0;
            bool blnIsFree = true;

            DataRow dr = null;
            SqlCommand _sql = _sqlDataAccess.GetCommand(execCommand, CommandType.StoredProcedure);

            _sql.Parameters.Add(new SqlParameter("EmailAddress", accountModel.EmailAddress));
            _sql.Parameters.Add(new SqlParameter("Password", accountModel.Password));


            DataTable dataTable = _sqlDataAccess.Execute(_sql);
            bool blnIsLoogedIn = false;


            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                blnIsLoogedIn = true;

                //dr = dataTable.Rows[0];

                HttpCookie objCookie = HttpContext.Current.Request.Cookies["UserInfo"];

                if (objCookie == null)
                {
                    HttpCookie cookie = new HttpCookie("UserInfo", Newtonsoft.Json.JsonConvert.SerializeObject(dataTable));
                    cookie.Expires = DateTime.Now.AddMinutes(10);
                    HttpContext.Current.Response.Cookies.Add(cookie);

                    //var cookie = new HttpCookie("UserInfo", Newtonsoft.Json.JsonConvert.SerializeObject(dr));
                    //{
                    //    Expires = DateTime.Now.AddMinutes(30)
                    //};
                }
                else
                {
                    objCookie.Expires = DateTime.Now.AddMinutes(10);
                    objCookie.Value = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                    HttpContext.Current.Response.Cookies.Add(objCookie);
                }
                

                //HttpContext.Current.Session["UserInfo"] = dataTable.Rows[0];
            }

            string strReturnValue = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                LoggedIn = blnIsLoogedIn
            });

            return Ok(strReturnValue);
        }

        [Route("api/Account/ChangePassword")]
        [HttpPost]
        public IHttpActionResult ChangePassword(AccountModel accountModel)
        {
            SqlDataAccess _sqlDataAccess = new SqlDataAccess();

            string execCommand = "sp_Account_Change_Password";
            int intID = 0;
            string strShowResults = "0";
            int intUserRemainingCredits = 0;
            bool blnIsFree = true;

            DataRow dr = null;
            SqlCommand _sql = _sqlDataAccess.GetCommand(execCommand, CommandType.StoredProcedure);

            _sql.Parameters.Add(new SqlParameter("EmailAddress", accountModel.EmailAddress));
            _sql.Parameters.Add(new SqlParameter("Password", accountModel.Password));
            _sql.Parameters.Add(new SqlParameter("NewPassword", accountModel.NewPassword));


            DataTable dataTable = _sqlDataAccess.Execute(_sql);
            bool blnIsLoogedIn = false;


            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                if (Convert.ToInt32(dataTable.Rows[0][0]) == 1)
                {
                    blnIsLoogedIn = true;
                }
            }

            string strReturnValue = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                ChangePassword = blnIsLoogedIn
            });

            return Ok(strReturnValue);
        }


        [Route("api/Account/ForgotPassword")]
        [HttpPost]
        public IHttpActionResult ForgotPassword(AccountModel accountModel)
        {
            SqlDataAccess _sqlDataAccess = new SqlDataAccess();

            string execCommand = "sp_Account_IsUserNameExists";
            int intID = 0;
            string strShowResults = "0";
            int intUserRemainingCredits = 0;
            bool blnIsFree = true;

            DataRow dr = null;
            SqlCommand _sql = _sqlDataAccess.GetCommand(execCommand, CommandType.StoredProcedure);

            string strRandomPassword = GetRandomPassword();


            _sql.Parameters.Add(new SqlParameter("EmailAddress", accountModel.EmailAddress));
            _sql.Parameters.Add(new SqlParameter("Password", strRandomPassword));
            


            DataTable dataTable = _sqlDataAccess.Execute(_sql);
            bool blnIsLoogedIn = false;


            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                string strEmailAddress = "";
                strEmailAddress = dataTable.Rows[0]["EmailAddress"].ToString();

                blnIsLoogedIn = true;

                using (SmtpClient client = new SmtpClient("smtp.gmail.com", 587))
                {
                    try
                    {
                        bool emailsent = false;
                        using (MailMessage mail = new MailMessage())
                        {
                            mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString(), "KeywordPlanner");
                            mail.To.Add(strEmailAddress);
                            mail.Subject = "Keyword Planner Forgot Password";
                            mail.Body = GetEmailTemplate(strRandomPassword);
                            mail.IsBodyHtml = true;
                            //mail.Attachments.Add(new Attachment("C:\\file.zip"));

                            using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                            {
                                smtp.UseDefaultCredentials = false;
                                //smtp.Credentials = new NetworkCredential("ppcexpokeywordplanner@gmail.com", "ABC123ssi");
                                smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());

                                smtp.EnableSsl = true;
                                //smtp.UseDefaultCredentials = true;
                                smtp.Send(mail);
                                emailsent = true;
                            }


                        }
                    }
                    catch (Exception e)
                    {
                        //throw new ApplicationException(e.Message);
                        throw e;
                    }
                }
            }

            string strReturnValue = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                ChangePassword = blnIsLoogedIn
            });

            return Ok(strReturnValue);
        }

        [Route("api/Account/CheckUser")]
        [HttpGet]
        public IHttpActionResult CheckUser()
        {
            DataTable dt = null;

            HttpCookie objCookie = HttpContext.Current.Request.Cookies["UserInfo"];

            if (objCookie != null)
            {
                dt = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTable>(objCookie.Value);
            }

            //if (HttpContext.Current.Session["UserInfo"] != null)
            //{
            //    dr = (DataRow)HttpContext.Current.Session["UserInfo"];
            //}

            string strReturnValue = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                UserInfo = dt
            });

            return Ok(strReturnValue);
        }

        [Route("api/Account/LogOut")]
        [HttpGet]
        public IHttpActionResult LogOut()
        {
            HttpCookie objCookie = HttpContext.Current.Request.Cookies["UserInfo"];

            if (objCookie != null)
            {
                objCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(objCookie);
            }


            return Ok();
        }

        private string GetEmailTemplate(string strNewPassword)
        {
            string strTemplate = "Hi, <br /> A request has been received to reset your password for account. <br />Your new password is: " + strNewPassword;
            strTemplate += "<br /><br /> Thank you, <br /> The Keyword Planner Team";


            return strTemplate;
        }

        private string GetRandomPassword()
        {
            var passwordBuilder = new StringBuilder();

            // 4-Letters lower case   
            passwordBuilder.Append(RandomString(4, true));

            // 4-Digits between 1000 and 9999  
            passwordBuilder.Append(RandomNumber(1000, 9999));

            // 2-Letters upper case  
            passwordBuilder.Append(RandomString(2));
            return passwordBuilder.ToString();
        }

        private int RandomNumber(int min, int max)
        {
            Random _random = new Random();
            return _random.Next(min, max);
        }

        private string RandomString(int size, bool lowerCase = false)
        {
            Random _random = new Random();

            var builder = new StringBuilder(size);

            // Unicode/ASCII Letters are divided into two blocks
            // (Letters 65–90 / 97–122):
            // The first group containing the uppercase letters and
            // the second group containing the lowercase.  

            // char is a single Unicode character  
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26; // A...Z or a..z: length=26  

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }
    }
}
