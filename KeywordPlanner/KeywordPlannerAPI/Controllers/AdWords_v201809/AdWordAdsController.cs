﻿using KeywordPlannerAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TargetIdeas.AdWords_v201809;

namespace KeywordPlannerAPI.Controllers.AdWords_v201809
{
    public class AdWordAdsController : ApiController
    {
        /// <summary>
        /// {"ExpandedAdModel":{"AdWordUser":"","AdGroupId":"string id of adgroup","Ads":[{"HeadLinePart1":"","HeadLinePart2":"","HeadLinePart3":"","Description1":"","Description2":"","FinalUrls":["Url1","Url2","Url3"]},{"HeadLinePart1":"","HeadLinePart2":"","HeadLinePart3":"","Description1":"","Description2":"","FinalUrls":["Url1","Url2","Url3"]}],"Keywords":["Keyword1","Keyword2","Keyword3"]}}
        /// </summary>
        /// <param name="expandedAdModel"> AdModel </param>
        /// <returns></returns>
        public IHttpActionResult POST(ExpandedAdModel expandedAdModel)
        {
            try
            {
                bool expandedAdModelResult = new AddExpandedTextAds().addExpandedTextAds(expandedAdModel.User, expandedAdModel.AdGroupId, expandedAdModel.Ads, expandedAdModel.Keywords);
                return Ok(expandedAdModelResult);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
