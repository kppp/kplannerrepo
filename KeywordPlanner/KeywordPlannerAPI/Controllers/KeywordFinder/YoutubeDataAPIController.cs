﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Google.Api.Ads.AdWords.v201809;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using KeywordPlannerParser.Model;
using KeywordPlannerParser.Parser;
using KeywordPlannerParser.Parser.GooglePrepositionParser;
using KeywordPlannerParser.Parser.GoogleQuestionParser;

namespace KeywordPlannerAPI.Controllers.KeywordFinder
{
    public class YoutubeDataAPIController : ApiController
    {
        public IHttpActionResult Get(string YoutubeSearchKeyword, long languageId, long locationId, bool targetGoogleSearch, bool targetSearchNetwork, [FromUri]string excludeKeywordsList, bool isKeyword,
            string searchVolumeMin, string searchVolumeMax, string cpcMin, string cpcMax, string competitionMin, string competitionMax, bool includeChartData = false)
        {
            //remove duplicates from the keywordsList
            //string[] KeyArrayParts = keyArray.Trim().Split(',');
            //KeyArrayParts = KeyArrayParts.Distinct().ToList().ToArray();

            //remove duplicates from the excludeKeywordsList



            try
            {
                /********************************** Removing Youtube Searching **********************************************/

                /*
                var youtubeService = new YouTubeService(new BaseClientService.Initializer()
                {
                    ApiKey = System.Configuration.ConfigurationManager.AppSettings["YoutubeDataAPIKey"].ToString(),
                    ApplicationName = System.Configuration.ConfigurationManager.AppSettings["YoutubeDataAPPName"].ToString()
                });

                var strOrignalSearchTerm = YoutubeSearchKeyword.Substring(0, YoutubeSearchKeyword.IndexOf(","));
                var strSuggestions = YoutubeSearchKeyword.Substring(YoutubeSearchKeyword.IndexOf(",") + 1);

                var searchListRequest = youtubeService.Search.List("snippet");
                searchListRequest.Q = strOrignalSearchTerm; // Replace with your search term.
                searchListRequest.MaxResults = 50;
                searchListRequest.Type = "video";
                searchListRequest.VideoCaption = SearchResource.ListRequest.VideoCaptionEnum.ClosedCaption;

                // Call the search.list method to retrieve results matching the specified query term.
                //var searchListResponse = await searchListRequest.ExecuteAsync();
                var searchListResponse = searchListRequest.Execute();
                */




                //youtubeService.Videos.List()

                //List<string> lstKeywords = new List<string>();
                //List<string> videos = new List<string>();
                //List<string> channels = new List<string>();
                //List<string> playlists = new List<string>();

                // Add each result to the appropriate list, and then display the lists of
                // matching videos, channels, and playlists.

                //List<GridModel> lstGridModel = new List<GridModel>();

                //string strVideoIds = "";
                //List<string> lstDescriptions = new List<string>();
                //foreach (var searchResult in searchListResponse.Items)
                //{
                //    GridModel objGridModel = new GridModel();

                //    lstKeywords.Add(searchResult.Snippet.Title);

                //    lstDescriptions.Add(searchResult.Snippet.Description);
                //}



                //dynamic objResult = GetTargetIdeaObjects(string.Join(",", lstKeywords), 2, languageId, locationId, targetGoogleSearch, targetSearchNetwork, excludeKeywordsList, isKeyword);
                //dynamic objResultSuggestion = GetTargetIdeaObjects(strSuggestions, 3, languageId, locationId, targetGoogleSearch, targetSearchNetwork, excludeKeywordsList, isKeyword);




                //var contentResult = result as OkNegotiatedContentResult<dynamic>;


                //Console.WriteLine(String.Format("Videos:\n{0}\n", string.Join("\n", videos)));
                //Console.WriteLine(String.Format("Channels:\n{0}\n", string.Join("\n", channels)));
                //Console.WriteLine(String.Format("Playlists:\n{0}\n", string.Join("\n", playlists)));

                //return result;
                //return Ok(((System.Web.Http.Results.OkNegotiatedContentResult<System.Collections.Generic.List<KeywordPlannerParser.Model.GridModel>>)result).Content);

                dynamic objResultSuggestion = GetTargetIdeaObjects(YoutubeSearchKeyword, 2, languageId, locationId, targetGoogleSearch, targetSearchNetwork, excludeKeywordsList, isKeyword,
                    searchVolumeMin, searchVolumeMax, cpcMin, cpcMax, competitionMin, competitionMax, includeChartData);

                //return Ok(new { SuggestionsData = objResultSuggestion, Result = objResult });
                return Ok(new { Result = objResultSuggestion});
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private dynamic GetTargetIdeaObjects(string keyArray, int ideaType, long languageId, long locationId, bool targetGoogleSearch, bool targetSearchNetwork, [FromUri]string excludeKeywordsList, bool isKeyword,
            string searchVolumeMin, string searchVolumeMax, string cpcMin, string cpcMax, string competitionMin, string competitionMax, bool includeChartData = false)
        {
            //remove duplicates from the keywordsList
            string[] KeyArrayParts = keyArray.Trim().Split(',');
            KeyArrayParts = KeyArrayParts.Distinct().ToList().ToArray();

            //remove duplicates from the excludeKeywordsList
            string[] excludeKeyArrayParts = { };
            if (excludeKeywordsList != null && excludeKeywordsList.Length > 0)
            {
                excludeKeyArrayParts = excludeKeywordsList.Trim().Split(',');
                excludeKeyArrayParts = excludeKeyArrayParts.Distinct().ToList().ToArray();
            }

            try
            {
                int intMinSearchVolume = ParseIntValues(searchVolumeMin);
                int intMaxSearchVolume = ParseIntValues(searchVolumeMax, true);

                if (ideaType == 3)
                {
                    NameValueCollection ClientCustomersCollection = (NameValueCollection)ConfigurationManager.GetSection("ClientCustomers");
                    TargetingIdeaPage targetIdeaPage = new BLReport().PopulateCompetitionData(ClientCustomersCollection["Umash Marketing"], KeyArrayParts, ideaType, languageId, locationId, targetGoogleSearch, targetSearchNetwork, excludeKeyArrayParts, isKeyword);
                    if (targetIdeaPage.entries == null)
                    {
                        List<GridModel> gridModel = new List<GridModel>();
                        gridModel.Add(new GridModel());
                        gridModel[0].ID = -1;
                        return gridModel;
                    }
                    else
                    {
                        ModelParser p = new GridParser();
                        List<GridModel> gridModel = p.Parse(targetIdeaPage);
                        gridModel[0].ID = -1;
                        return gridModel;
                    }
                }
                else if (ideaType == 2)
                {
                    NameValueCollection ClientCustomersCollection = (NameValueCollection)ConfigurationManager.GetSection("ClientCustomers");
                    /// Search Volume will be filtered on search level
                    TargetingIdeaPage targetIdeaPage = new BLReport().PopulateCompetitionData(ClientCustomersCollection["Umash Marketing"], KeyArrayParts, ideaType, languageId, locationId, targetGoogleSearch, targetSearchNetwork, excludeKeyArrayParts, isKeyword, intMinSearchVolume, intMaxSearchVolume);
                    if (targetIdeaPage.entries == null)
                    {
                        return
                        new
                        {
                            GridModelList = new GridModel(),
                            GridBarModel = new GridBarModel()
                        };
                    }
                    else
                    {
                        ModelParser p = new GridParser();


                        decimal flMinCPC = ParseDecimalValues(cpcMin);
                        decimal flMaxCPC = ParseDecimalValues(cpcMax, true);
                        int intMinCompetition = ParseIntValues(competitionMin);
                        int intMaxCompetition = ParseIntValues(competitionMax, true);

                        /// CPC and Competitio will be filtered at parsing level

                        List<GridModel> gridModel = p.Parse(targetIdeaPage, intMinSearchVolume, intMaxSearchVolume, flMinCPC, flMaxCPC, intMinCompetition, intMaxCompetition);



                        //gridModel = gridModel.Where(x => (x.SearchVolume >= intMinSearchVolume && x.SearchVolume <= intMaxSearchVolume)).ToList();
                        //gridModel = gridModel.Where(x => (x.AverageCPC >= flMinCPC && x.AverageCPC <= flMaxCPC)).ToList();
                        //gridModel = gridModel.Where(x => (x.Competition >= intMinCompetition && x.Competition <= intMaxCompetition)).ToList();



                        List<GridModel> gridModelListData = gridModel.ToList();
                        ModelParser objGridBarParser = new GridBarParser();
                        GridBarModel gridBarModel = objGridBarParser.Parse(gridModel);
                        List<String> Question = new QuestionGridParser().QuestionGridModel(gridModel.Select(m => m.KeywordText).ToList());
                        List<String> Preposition = new PrepositionGridParser().PrepositionGridModel(gridModel.Select(m => m.KeywordText).ToList());

                        for (int i = 0; i < gridModel.Count; i++)
                        {
                            for (int j = 0; j < Question.Count; j++)
                            {
                                if (gridModel[i].KeywordText.Equals(Question[j]))
                                {
                                    gridModel.RemoveAt(i);
                                    i = i - 1;
                                    break;
                                }
                            }

                            if (includeChartData == false)
                            {
                                gridModel[i].MonthlyTargetedSearches = new MonthlyTargetedSearches[0];
                                gridModel[i].Xaxis = new string[0];
                                gridModel[i].Yaxis = new long[0];
                            }
                        }
                        if (isKeyword == true)
                        {
                            return
                            new
                            {
                                GridModelListRelevant = gridModel.Where(m => m.KeywordText.ToUpper().Contains(keyArray.ToUpper())),
                                GridModelListRelated = gridModel.Where(m => !(m.KeywordText.ToUpper().Contains(keyArray.ToUpper()))),
                                Questions = Question,
                                Prepositions = Preposition,
                                GridBarModel = gridBarModel,
                                GridModel = gridModelListData
                            };
                        }
                        else
                        {
                            return
                            new
                            {
                                GridModelListRelevant = gridModel.Where(m => !(m.KeywordText.ToUpper().Contains(keyArray.ToUpper()))),
                                Questions = Question,
                                Prepositions = Preposition,
                                GridBarModel = gridBarModel,
                                GridModel = gridModelListData
                            };
                        }

                    }
                }
                else if (ideaType == 1)
                {
                    NameValueCollection ClientCustomersCollection = (NameValueCollection)ConfigurationManager.GetSection("ClientCustomers");
                    TargetingIdeaPage targetIdeaPage = new BLReport().PopulateCompetitionData(ClientCustomersCollection["Umash Marketing"], KeyArrayParts, ideaType, languageId, locationId, targetGoogleSearch, targetSearchNetwork, excludeKeyArrayParts, isKeyword);
                    if (targetIdeaPage.entries == null)
                    {
                        return new GridBarModel();
                    }
                    else
                    {
                        ModelParser p = new GridBarParser();
                        GridBarModel gridBarModel = p.Parse(targetIdeaPage);
                        return gridBarModel;
                    }
                }
                else
                {
                    return BadRequest("Parameters are incorrect");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private int ParseIntValues(string strValue, bool blnIsMax = false)
        {
            int intLocalValue = 0;

            //if (blnIsMax) intLocalValue = 9999999;

            if (!int.TryParse(strValue, out intLocalValue))
            {
                if (blnIsMax) intLocalValue = 9999999;
            }
            return intLocalValue;
        }

        private decimal ParseDecimalValues(string strValue, bool blnIsMax = false)
        {
            decimal intLocalValue = 0;

            if (blnIsMax) intLocalValue = 9999999;

            if (!decimal.TryParse(strValue, out intLocalValue))
            {
                if (blnIsMax) intLocalValue = 9999999;
            }
            return intLocalValue;
        }

    }
}

