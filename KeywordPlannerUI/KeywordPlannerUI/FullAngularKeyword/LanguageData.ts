export default class LanguageData {
    CriterionId: number;
    LanguageCode: string;
    LanguageName: string;
};