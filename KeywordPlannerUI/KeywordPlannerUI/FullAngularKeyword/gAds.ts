export default class GoogleAdsData {
    HeadLinePart1: string = "";
    HeadLinePart2: string = "";
    HeadLinePart3: string = "";
    Description1: string = "";
    Description2: string = "";
    FinalUrls: string = "";
  };