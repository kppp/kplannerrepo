export class LoaderState {
    color = 'primary';
    mode = 'indeterminate';
    value = 50;
  }