import { AuthConfig } from 'angular-oauth2-oidc';
 
export const authConfig: AuthConfig = {
 
  // Url of the Identity Provider
  //issuer: 'https://www.googleapis.com/auth/adwords',
  
  issuer: 'https://accounts.google.com',

  // URL of the SPA to redirect the user to after login
  // redirectUri: window.location.origin + '/index.html',
  redirectUri: window.location.origin + '/silentrefresh',
//   silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
  
  // The SPA's id. The SPA is registered with this id at the auth-server
  clientId: '364567748615-3drckutorlosa8l6q10mscv51ammto3s.apps.googleusercontent.com',
 
  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  //scope: 'openid profile email',
  scope: 'https://www.googleapis.com/auth/adwords',
  strictDiscoveryDocumentValidation: false,
  tokenEndpoint: '',
    // loginUrl: 'https://www.googleapis.com/oauth2/authorize',
    // redirectUri: window.location.origin + '/SSO',
    // clientId: '364567748615-3drckutorlosa8l6q10mscv51ammto3s.apps.googleusercontent.com',
    // scope: 'guilds connections identify',
    // strictDiscoveryDocumentValidation: false,
    // responseType: 'token',
    // oidc: false,
    
}