export interface DialogData {
    PickedKeywords: string[];
    UserData: any;
}