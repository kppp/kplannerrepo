// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiURL: "http://localhost/KeywordPlannerAPI/api",
  //apiURL: "http://192.168.192.175/KeywordPlannerAPI/api",
  //trendapiURL: "http://localhost:3000/api/",
  trendapiURL: "https://testnodeapp002.herokuapp.com/api/",
  authURL: "http://localhost/KeywordPlannerAPI/",
  autoSuggestURL: "http://suggestqueries.google.com/complete/search?callback=JSON_CALLBACK&client=firefox&hl=en&q=",
  //authURL: "http://192.168.192.175/KeywordPlannerAPI/"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
