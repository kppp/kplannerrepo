export const environment = {
  production: true,
  //apiURL: "http://server.keywordplanners.io/api/",
  apiURL: "http://www.keywordplanners.io/KeywordPlannerAPI/api/",
  trendapiURL: "https://testnodeapp002.herokuapp.com/api/",
  authURL: "http://www.keywordplanners.io/KeywordPlannerAPI/",
  autoSuggestURL: "http://suggestqueries.google.com/complete/search?callback=JSON_CALLBACK&client=firefox&hl=en&q=",
  //authURL: "http://server.keywordplanners.io/"
};
