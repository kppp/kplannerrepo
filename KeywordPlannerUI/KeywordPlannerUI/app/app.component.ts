
import { Component, OnInit, OnDestroy, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { AccountopService } from './services/accountop.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Keyword Tool for Google Ads';
  PickedKeyWords : string[] = [];
  isUserLoggedIn: boolean = false;
  emailAddress: string = "";
  constructor(private accountService: AccountopService) { 
  
  }

  ngOnInit() {
    this.checkUserIsLoggedIn();
  }
  
  checkUserIsLoggedIn(){
    this.accountService.checkUserLoggedIn().subscribe((data:any) => {
      data = JSON.parse(data);
      if(data.UserInfo == null){
        this.isUserLoggedIn = false;
        //this.accountService.setSignIn(false);
      }
      else{
        this.isUserLoggedIn = true;
        this.emailAddress = data.UserInfo[0].EmailAddress;
        //this.accountService.setSignIn(true);
      }
      //
    });
  }

  onLogout(){
    this.accountService.logOff().subscribe(() => {
      window.location.href = "/";  
    });
  }
}
